from fastapi import FastAPI, Response


app = FastAPI()

cursos = {
    1:   {'titulo': 'Programação para Leigos', 'aulas': 112, 'horas': 58},
    2: {'titulo': 'Python Básico', 'aulas': 50, 'horas': 12}
}

@app.get('/cursos/')
def get_cursos():
    return {"oi": "231"}

if __name__ == '__main__':
    import uvicorn
    uvicorn.run('app:app', host='0.0.0.0', port=8000, debug=True, reload=True)