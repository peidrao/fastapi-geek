from typing import List
from fastapi import APIRouter, Depends, HTTPException, Response
from starlette import status

from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.future import select

from models.course import Course
from schemas.course import CourseSchema
from core.deps import get_session


router = APIRouter()

@router.post('/', status_code=status.HTTP_201_CREATED, response_model=CourseSchema)
async def post(course: CourseSchema, db: AsyncSession = Depends(get_session)):
    course_ = Course(title=course.title, classes=course.classes, hours=course.classes)
    db.add(course_)
    await db.commit()
    return course


@router.get('/', response_model=List[CourseSchema])
async def get_all(db: AsyncSession = Depends(get_session)):
    async with db as session:
        query = select(Course)
        result = await session.execute(query)
        courses : List[Course] = result.scalars().all()
        return courses


@router.get('/{id}', response_model=CourseSchema, status_code=status.HTTP_200_OK)
async def get(id: int, db: AsyncSession = Depends(get_session)):
    async with db as session:
        query = select(Course).filter(Course.id == id)
        result = await session.execute(query)
        course = result.scalars().one_or_none()
        if course:
            return course
        raise HTTPException(detail='course not found', status_code=status.HTTP_404_NOT_FOUND)


@router.put('/{id}', response_model=CourseSchema, status_code=status.HTTP_202_ACCEPTED)
async def put(id: int, request: CourseSchema, db: AsyncSession = Depends(get_session)):
    async with db as session:
        query = select(Course).filter(Course.id == id)
        result = await session.execute(query)
        course = result.scalars().one_or_none()
        if course:
            course.title = request.title
            course.classes = request.classes
            course.hours = request.hours
            await session.commit()

            return course
        raise HTTPException(detail='course not found', status_code=status.HTTP_404_NOT_FOUND)


@router.delete('/{id}', status_code=status.HTTP_204_NO_CONTENT)
async def delete(id: int, db: AsyncSession = Depends(get_session)):
    async with db as session:
        query = select(Course).filter(Course.id == id)
        result = await session.execute(query)
        course = result.scalars().one_or_none()
        if course:
            await session.delete(course)
            await session.commit()
            return Response(status_code=status.HTTP_204_NO_CONTENT)
        raise HTTPException(detail='course not found', status_code=status.HTTP_404_NOT_FOUND)