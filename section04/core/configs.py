from typing import List
from pydantic import BaseSettings, AnyHttpUrl
from sqlalchemy.ext.declarative import declarative_base


class Settings(BaseSettings):
    API_VERSION: str = '/api/v1'
    DB_URL: str = 'postgresql+asyncpg://root:password@localhost:5434/new_database'
    DBBaseModel = declarative_base()

    class Config:
        case_sensite = True
    
settings = Settings()