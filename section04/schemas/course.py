from typing import Optional 
from pydantic import BaseModel


class CourseSchema(BaseModel):
    id: Optional[int]
    title: Optional[str]
    classes: Optional[int]
    hours: Optional[int]

    class Config:
        orm_mode = True